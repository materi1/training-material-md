1. create new directory **bioskop** under myfirstweb.

2. get inside bioskop and open vscode.

3. create *.html file **homepage.html**

4. edit homepage.html

>
    <!DOCTYPE html>
    <html>

    <head>
    <title>Bioskop</title>
    </head>

    <body>
    <!-- external link(protocol+google), internal link / relative url (lokasi penyimpanan)  -->
    <h3>Selamat Datang di Bioskop Kesayangan Anda</h3>
    <p>Film yang sedang tayang - tayang uluuhh tayaaang tayang <a href="../film/deskripsifilm.html#bagian1">Read
            More...</a> </p>

    <p>Klik <a href="../film/film.html">disini</a> untuk melihat film...</p>

    <!-- dilan -->
    <!-- <p>Klik <a href="../film/deskripsifilm.html#bagian1">disini</a> untuk film dilan...</p>  -->
    </body>

    </html>




5. open homepage.html from browser