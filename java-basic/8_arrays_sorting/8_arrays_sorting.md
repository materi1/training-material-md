1. Buka `CMD`

2. masuk ke `Drive D:/`

3. mkdir __materi__ , masuk ke folder __materi__

4. mkdir **8_arrays_sorting**

5. masuk ke folder **8_arrays_sorting**

6. buka `vscode`
>
    D:\materi\8_arrays_sorting> code .

 *jangan tutup CMD*

7. buat file `ArraysSorting.java`

8. tulis `code` di bawah ini

---

```java
    public class ArraysSorting {

    public static void main(String[] args) {

        int[] numbers = { 19, 30, 29, 51, 42, 732, 7, 2, 6, 52, 34 };
        int temp;

        System.out.println("Di bawah ini adalah numbers sebelum di sorting");

        for (int num : numbers) {
            System.out.println(num);
        }

        System.out.println("Di bawah ini adalah numbers setelah di sorting Ascending");

        for (int i = 1; i < numbers.length; i++) {
            for (int j = i; j > 0; j--) {
                if (numbers[j] < numbers[j - 1]) {
                    temp = numbers[j];
                    numbers[j] = numbers[j - 1];
                    numbers[j - 1] = temp;
                }
            }
        }

        for (int num : numbers) {
            System.out.println(num);
        }

    }
    }
```
---

9. kembali ke cmd, build file java
>
    javac ArraysSorting.java

10. lihat ke dalam folder **8_arrays_sorting**, pastikan muncul file `ArraysSorting.class`
>
    munculnya ArraysSorting.class adalah proses compiling yang dilakukan oleh compiler java

11. kembali ke cmd, jalankan class java
>
    java ArraysSorting


___

