1. Buka `CMD`

2. masuk ke `Drive D:/`

3. mkdir __materi__ , masuk ke folder __materi__

4. mkdir **8_arrays_list**

5. masuk ke folder **8_arrays_list**

6. buka `vscode`
>
    D:\materi\8_arrays_list> code .

 *jangan tutup CMD*

7. buat file `ArrayLists.java`

8. tulis `code` di bawah ini

---

```java
    import java.util.ArrayList;

    public class ArrayLists {

    public static void main(String[] args) {

    ArrayList<String> cars = new ArrayList<String>();
    cars.add("Volvo");
    cars.add("BMW");
    cars.add("Ford");
    cars.add("Mazda");
    System.out.println(cars);
    }

    }
```
---

9. kembali ke cmd, build file java
>
    javac ArrayLists.java

10. lihat ke dalam folder **8_arrays_list**, pastikan muncul file `ArrayLists.class`
>
    munculnya ArrayLists.class adalah proses compiling yang dilakukan oleh compiler java

11. kembali ke cmd, jalankan class java
>
    java ArrayLists


___

