1. Buka `CMD`

2. masuk ke `Drive D:/`

3. mkdir __materi__ , masuk ke folder __materi__

4. mkdir **4_Modulus**

5. masuk ke folder **4_Modulus**

6. buka `vscode`
>
    D:\materi\4_Modulus> code .

 *jangan tutup CMD*

7. buat file `Modulus.java`

8. tulis `code` di bawah ini

---

```java
    public class Modulus {

    public static void main(String[] args) {

        int angka1 = Integer.parseInt(args[0]);

        int hasil = angka1 % 4;

        System.out.println(hasil);

        }
    }
```
---

9. kembali ke cmd, build file java
>
    javac Kalkulator.java

10. lihat ke dalam folder __4_Modulus__, pastikan muncul file `Modulus.class`
>
    munculnya Modulus.class adalah proses compiling yang dilakukan oleh compiler java

11. kembali ke cmd, jalankan class java
>
    java Modulus 10
___

