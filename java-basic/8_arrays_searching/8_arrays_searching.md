1. Buka `CMD`

2. masuk ke `Drive D:/`

3. mkdir __materi__ , masuk ke folder __materi__

4. mkdir **8_arrays_searching**

5. masuk ke folder **8_arrays_searching**

6. buka `vscode`
>
    D:\materi\8_arrays_searching> code .

 *jangan tutup CMD*

7. buat file `ArraysSearching.java`

8. tulis `code` di bawah ini

---

```java
    public class ArraysSearching {

    public static void main(String[] args) {
        String[] cars = { "Volvo", "BMW", "Ford", "Mazda" };
        int found = 0;

        for (int i = 0; i < cars.length; i++) {

            if (cars[i].equals("Ford")) {
                found = i;

            }
        }

        if (found > 0) {
            System.out.println("Ford is in index " + found);
        }

    }
    }
```
---

9. kembali ke cmd, build file java
>
    javac ArraysSearching.java

10. lihat ke dalam folder **8_arrays_searching**, pastikan muncul file `ArraysSearching.class`
>
    munculnya ArraysSearching.class adalah proses compiling yang dilakukan oleh compiler java

11. kembali ke cmd, jalankan class java
>
    java ArraysSearching


___

