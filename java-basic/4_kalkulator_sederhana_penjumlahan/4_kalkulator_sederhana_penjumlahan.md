1. Buka `CMD`

2. masuk ke `Drive D:/`

3. mkdir __materi__ , masuk ke folder __materi__

4. mkdir __4_kalkulator_sederhana__

5. masuk ke folder __4_kalkulator_sederhana__

6. buka `vscode`
>
    D:\materi\4_kalkulator_sederhana> code .

 *jangan tutup CMD*

7. buat file `Kalkulator.java`

8. tulis `code` di bawah ini

---

```java
    public class Kalkulator {

    public static void main(String[] args) {

        String perintah = args[0];

        int parameter1 = Integer.parseInt(args[1]);

        int parameter2 = Integer.parseInt(args[2]);

        int hasil = parameter1 + parameter2;

        System.out.println(parameter1 + " di" + perintah + " " + parameter2 + " adalah " + hasil);
        }
    }
```
---

9. kembali ke cmd, build file java
>
    javac Kalkulator.java

10. lihat ke dalam folder __4_kalkulator_sederhana__, pastikan muncul file `Kalkulator.class`
>
    munculnya Kalkulator.class adalah proses compiling yang dilakukan oleh compiler java

11. kembali ke cmd, jalankan class java
>
    java Kalkulator tambah 2 3

12. anda bisa mengganti tanda + menjadi `-, /,*,%`
___

