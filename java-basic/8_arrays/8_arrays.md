1. Buka `CMD`

2. masuk ke `Drive D:/`

3. mkdir __materi__ , masuk ke folder __materi__

4. mkdir **8_arrays**

5. masuk ke folder **8_arrays**

6. buka `vscode`
>
    D:\materi\8_arrays> code .

 *jangan tutup CMD*

7. buat file `Arrays.java`

8. tulis `code` di bawah ini

---

```java
    public class Arrays {

    public static void main(String[] args) {
        String[] cars = { "Volvo", "BMW", "Ford", "Mazda" };
        int[] myNum = { 10, 20, 30, 40 };

        System.out.println("Di bawah ini adalah isi dari array cars");

        for (String car : cars) {
            System.out.println(car);
        }

        System.out.println("Di bawah ini adalah index dan isi dari array cars");

        for (int i = 0; i < cars.length; i++) {
            System.out.println("Array dengan index " + i + " berisi " + cars[i]);
        }

        System.out.println("Di bawah ini adalah isi dari array myNum");

        for (int num : myNum) {
            System.out.println(num);
        }

        System.out.println("Di bawah ini adalah index dan isi dari array myNum");

        for (int j = 0; j < myNum.length; j++) {
            System.out.println("Array dengan index " + j + " berisi " + myNum[j]);
        }

    }
    }
```
---

9. kembali ke cmd, build file java
>
    javac Arrays.java

10. lihat ke dalam folder **8_arrays**, pastikan muncul file `Arrays.class`
>
    munculnya Arrays.class adalah proses compiling yang dilakukan oleh compiler java

11. kembali ke cmd, jalankan class java
>
    java Arrays


___

