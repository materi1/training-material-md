1. Buka `CMD`

2. masuk ke `Drive D:/`

3. mkdir __materi__ , masuk ke folder __materi__

4. mkdir **8_arrays_basic**

5. masuk ke folder **8_arrays_basic**

6. buka `vscode`
>
    D:\materi\8_arrays_basic> code .

 *jangan tutup CMD*

7. buat file `ArraysBasic.java`

8. tulis `code` di bawah ini

---

```java
    public class ArraysBasic {

    public static void main(String[] args) {
        int[] numbers2;
        numbers2 = new int[] { 42, 55, 99 };

        for (int i : numbers2) {
            System.out.println(i);
        }

        System.out.println("Di bawah ini kita akan mengganti isi dari array numbers2 dengan index 2 ");

        numbers2[2] = 100;

        for (int i = 0; i < numbers2.length; i++) {
            System.out.println("Array dengan index " + i + " berisi " + numbers2[i]);
        }
    }
    }
```
---

9. kembali ke cmd, build file java
>
    javac ArraysBasic.java

10. lihat ke dalam folder **8_arrays_basic**, pastikan muncul file `ArraysBasic.class`
>
    munculnya ArraysBasic.class adalah proses compiling yang dilakukan oleh compiler java

11. kembali ke cmd, jalankan class java
>
    java ArraysBasic


___

