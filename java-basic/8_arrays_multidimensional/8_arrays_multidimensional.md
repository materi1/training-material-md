1. Buka `CMD`

2. masuk ke `Drive D:/`

3. mkdir __materi__ , masuk ke folder __materi__

4. mkdir **8_arrays_multidimensional**

5. masuk ke folder **8_arrays_multidimensional**

6. buka `vscode`
>
    D:\materi\8_arrays_multidimensional> code .

 *jangan tutup CMD*

7. buat file `ArrayMultidimensional.java`

8. tulis `code` di bawah ini

---

```java
    public class ArrayMultidimensional {

    public static void main(String[] args) {
        int[][] arr = { { 1, 2 }, { 3, 4 } };

        for (int i = 0; i < 2; i++)
            for (int j = 0; j < 2; j++)
                System.out.println("arr[" + i + "][" + j + "] = " + arr[i][j]);
    }
    }
```
---

9. kembali ke cmd, build file java
>
    javac ArrayMultidimensional.java

10. lihat ke dalam folder **8_arrays_multidimensional**, pastikan muncul file `ArrayMultidimensional.class`
>
    munculnya ArrayMultidimensional.class adalah proses compiling yang dilakukan oleh compiler java

11. kembali ke cmd, jalankan class java
>
    java ArrayMultidimensional


___

