1. Buka `CMD`

2. masuk ke `Drive D:/`

3. mkdir __materi__ , masuk ke folder __materi__

4. mkdir **7_kalkulator_beda_file**

5. masuk ke folder **7_kalkulator_beda_file**

6. buka `vscode`
>
    D:\materi\7_kalkulator_beda_file> code .

 *jangan tutup CMD*

7. buat file `Kalkulator.java`

8. tulis `code` di bawah ini

---

```java
    public class Kalkulator {

    public static void main(String[] args) {

        int angka1 = Integer.parseInt(args[0]);

        int angka2 = Integer.parseInt(args[1]);

        System.out.println(tambah(angka1, angka2));

    }

    static int tambah(int x, int y) {
        int hasil = x + y;

        return hasil;
    }

    }
```

9. buat file `Penjumlahan.java`

10. tulis `code` di bawah ini

---

```java
    public class Penjumlahan {

    public int tambah(int x, int y) {
        int hasil = x + y;

        return hasil;

    }
    }
```
---

11. kembali ke cmd, build file java
>
    javac Kalkulator.java Penjumlahan.java

12. lihat ke dalam folder **7_kalkulator_beda_file**, pastikan muncul file `Kalkulator.class`
>
    munculnya Kalkulator.class adalah proses compiling yang dilakukan oleh compiler java

13. kembali ke cmd, jalankan class java
>
    java Kalkulator 1 2


___

